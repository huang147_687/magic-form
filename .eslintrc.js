/**
 * 官方文档：https://cn.eslint.org/docs/user-guide/configuring
 */
 module.exports = {
  // 默认情况下，ESLint会在所有父级组件中寻找配置文件，一直到根目录。ESLint一旦发现配置文件中有 "root": true，它就会停止在父级目录中寻找
  root: true,
  // 使用 env 关键字指定你想启用的环境
  env: {
    // nodejs环境
    node: true
  },
  // 从基础配置中继承已启用的规则
  'extends': [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  rules: {
    // 构建时避免 console 和 debugger 被一起构建上去
    // 'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    // 'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
