import Vue from 'vue'
import axios from 'axios'
import router from './router'
import store from './store'
import App from './App.vue'
import ElementUI from 'element-ui'
// 剪切板
import VueClipboard from 'vue-clipboard2'
// 样式文件，需单独引入
import 'element-ui/lib/theme-chalk/index.css'
import MarkingForm from './components/MarkingForm/MarkingForm.vue'
import Dialog from './components/Dialog/Dialog.vue'
import DeleteAttr from './components/Dialog/DeleteAttr.vue'

// 引入样式
import './assets/css/index.css'
// vue 粘贴板复制插件
VueClipboard.config.autoSetContainer = true
Vue.use(VueClipboard)
Vue.component(Dialog.name, Dialog)
Vue.component(DeleteAttr.name, DeleteAttr)
// 注册我们写的组件
Vue.component(MarkingForm.name, MarkingForm)
// 注册element-ui
Vue.use(ElementUI, { size: 'small' })
Vue.prototype.axios = axios
/* eslint-disable */
// 构建Vue
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})