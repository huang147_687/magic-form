import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/pageCustom'
    },
    {
      path: '/pageCustom',
      name: 'pageCustom',
      component: (resolve) => require(['@views/PageCustom/PageCustom.vue'], resolve)
    },
    {
      path: '/pagePreview',
      name: 'PagePreview',
      component: (resolve) => require(['@views/PagePreview/PagePreview.vue'], resolve)
    }
  ]
})
